/*jshint -W024 */ //Stop complaining about reserved words on chai expect asserts
(function () {
    'use strict';

    describe('habit', function () {

        beforeEach(function () {
                Habit.destroy();
            });

        describe('Habit', function () {

            it('should be defined', function () {
                expect(Habit).to.exist;
            });

            describe('supply', function () {

                it('should be defined', function () {
                    expect(Habit.supply).to.exist;
                });

                it('should be exported to the global namespace as supply', function () {
                    expect(window.supply).to.exist;
                });

                it('should be exported to the global namespace as define', function () {
                    expect(window.define).to.exist;
                });

                it('should test that the exported function is really supply', function () {
                    supply('someModule', [], sinon.stub().returns({}));
                    expect(Habit.__context.modules['someModule']).to.exist;
                });

                it('should take 3 arguments', function () {
                    expect(Habit.supply).to.have.length(3);
                });

                it('should register a module with the name of the module (1st arg)', function () {
                    supply('aModule', [], function () {return true;});
                    expect(Habit.__context.modules['aModule']).to.exist;
                });

                it('should register modules lazily', function () {
                    var mod = sinon.stub().returns({});
                    supply('aModule', [], mod);
                    expect(Habit.__context.modules['aModule']).to.exist;
                    expect(mod).not.to.have.been.called;
                });

                it('should resolve ./ relative dependency names', function () {
                    var concreteDepedModule = {};
                    var mainModule = function (dep) {return function () {return dep;};};

                    supply('some/fancy/depedModule', [], sinon.stub().returns(concreteDepedModule));
                    supply('some/fancy/mainModule', ['./depedModule'], mainModule);

                    need(['some/fancy/mainModule'], function(mod) {
                        expect(mod()).to.equal(concreteDepedModule);
                    });
                });

                it('should resolve ../ relative dependency names', function () {
                    var concreteDepedModule = {};
                    var mainModule = function (dep) {return function () {return dep;};};

                    supply('some/depedModule', [], sinon.stub().returns(concreteDepedModule));
                    supply('some/really/fancy/mainModule', ['../../depedModule'], mainModule);

                    need(['some/really/fancy/mainModule'], function(mod) {
                        expect(mod()).to.equal(concreteDepedModule);
                    });
                });

                it('should throw an error if the ../ relative path goes beyond the root',
                    function () {
                    var shouldThrow = function () {supply('my/fancy/module', ['../../../crashy'], {});};
                    expect(shouldThrow).to.throw(Habit.HabitError);
                });

                it('should disolve the module and all its dependent modules when it is re-supplied', function () {
                    supply('moda', [], {});
                    supply('modb', ['moda'], sinon.stub());

                    var modb = need('modb');

                    supply('moda', [], {});
                    expect(Habit.__context.modules['modb'].__isResolved).to.be.false;
                });
            });

            describe('need', function () {

                it('should be defined', function () {
                    expect(Habit.need).to.exist;
                });

                it('should be exported to the global namespace as need', function () {
                    expect(window.need).to.exist;
                });

                it('should be exported to the global namespace as require', function () {
                    expect(window.require).to.exist;
                });

                it('should test that the exported function is really need', function (done) {
                    var concreteMod = {};
                    supply('someModule', [], sinon.stub().returns(concreteMod));
                    need(['someModule'], function (someModule) {
                        expect(someModule).to.equal(concreteMod);
                        done();
                    });
                });

                it('should take three arguments', function () {
                    expect(Habit.need).to.have.length(3);
                });

                it('should call the callback with the modules from the dependencies', function () {
                    var concreteDepedModule = {};
                    var callback = sinon.stub();
                    supply('depedModule', [], sinon.stub().returns(concreteDepedModule));
                    need(['depedModule'], callback);
                    expect(callback).to.have.been.calledWith(concreteDepedModule);
                });

                it('should only resolve each module once even if it is a dependency of more than one module',
                    function () {

                    var modFunc = sinon.stub();

                    supply('moda', [], modFunc);
                    supply('modb', ['moda'], function () {});
                    supply('modc', ['moda'], function () {});

                    need(['modb', 'modc'], function (m) {
                        expect(modFunc).to.have.been.calledOnce;
                    });
                });

                it('should throw a Habit error if a circular dependency is detected', function () {
                    supply('moda', ['modb'], sinon.stub());
                    supply('modb', ['moda'], sinon.stub());

                    var shouldThrow = function () {
                        need(['moda'], sinon.stub);
                    };

                    expect(shouldThrow).to.throw(Habit.HabitError);
                });

                it('should generate a reverse dependency mapping as it resolves modules', function () {
                    var modA = sinon.stub(), modB = {};
                    supply('modB', [], modB);
                    supply('modA', ['modB'], modA);

                    need(['modA'], sinon.stub());

                    expect(
                        Habit
                        .__context
                        .modules['modB']
                        .dependentModules
                        .map(function(module) {
                            return module.name;
                        })).to.include('modA');
                });

                it('should return a module directly when called with a single module name', function () {
                    var mymod = {};
                    supply('mymod', [], mymod);
                    expect(require('mymod')).to.eql(mymod);
                });

                it('should work with modules registered out of order', function () {
                    var modC = {}, modB = {}, modA = {};
                    var modBFactory = sinon.stub().returns(modB);
                    var modAFactory = sinon.stub().returns(modA);
                    supply('modA', ['modB'], modAFactory);
                    supply('modB', ['modC'], modBFactory);
                    supply('modC', [], modC);

                    var a = need('modA');

                    expect(modAFactory).to.have.been.calledWith(modB);
                    expect(modBFactory).to.have.been.calledWith(modC);
                    expect(a).to.equal(modA);

                });

                it('should call __context.cloneResolveAndCall when overrides are supplied', function () {
                    Habit.__context.cloneResolveAndCall = sinon.stub();
                    need(['a', 'b'], sinon.stub(), {a: {}});
                    expect(Habit.__context.cloneResolveAndCall).to.have.been.called;
                });

                it('should use overrides inside a need callback but not outside', function () {
                    supply('a', [], 'real');
                    supply('b', ['a'], function (a) {return a;});

                    need(['a'], function (a) {
                        expect(a).to.equal('injected');
                    }, {'a': 'injected'});

                    expect(need('a')).to.equal('real');

                });

                it('should resolve a shallow copy of modules which are object values', function () {
                    supply('mymod', [], {someVal: 'original'});

                    need(['mymod'], function (myMod) {
                        myMod.someVal = 'altered';
                    });

                    Habit.disolve('mymod');
                    expect(need('mymod').someVal).to.equal('original');

                });

                it('should resolve a copy of modules which are arrays', function () {
                    supply('mymod', [], [123]);

                    need(['mymod'], function (myMod) {
                        expect(Array.isArray(myMod)).to.be.true;
                        myMod[0] = 456;
                    });

                    Habit.disolve('mymod');
                    expect(need('mymod')[0]).to.equal(123);
                });

            });

            describe('Context', function () {

                it('should be defined', function () {
                    expect(Habit.Context).to.exist;
                });

                it('should have a modules property', function () {
                    var c = new Habit.Context();
                    expect(c.modules).to.exist;
                });

                describe('disolve', function () {
                    it('should exist', function () {
                        expect(Habit.__context.disolve).to.exist;
                    });

                    it('should call disolve on the named module', function () {
                        supply('amod', [], function () {});
                        Habit.__context.modules['amod'].disolve = sinon.stub();
                        Habit.disolve('amod');
                        expect(Habit.__context.modules['amod'].disolve).to.have.been.called;
                    });
                });

                describe('disolveAll', function () {
                    it('should exist', function () {
                        expect(Habit.__context.disolveAll).to.exist;
                    });

                    it('should disolve all the resolved modules', function () {
                        supply('a', [], {});
                        supply('b', [], {});
                        need(['a', 'b'], function () {});
                        expect(Habit.__context.modules['a'].__isResolved).to.be.true;
                        expect(Habit.__context.modules['b'].__isResolved).to.be.true;
                        Habit.__context.disolveAll();
                        expect(Habit.__context.modules['a'].__isResolved).to.be.false;
                        expect(Habit.__context.modules['b'].__isResolved).to.be.false;
                    });
                });

                describe('mock', function () {
                    it('should exist', function () {
                        expect(new Habit.Context().mock).to.exist;
                    });

                    it('should throw an error if the module requested for mocking does not exist',
                        function () {
                        var shouldThrow = function () {Habit.mock('I am not real', {});};
                        expect(shouldThrow).to.throw(Habit.HabitError);
                    });

                    it('should call mock on the module to be mocked', function () {
                        supply('aMod', [], {});
                        Habit.__context.modules['aMod'].mock = sinon.stub();
                        Habit.mock('aMod', {});
                        expect(Habit.__context.modules['aMod'].mock).to.have.been.called;
                    });
                });

                describe('unmock', function () {
                    it('should exist', function () {
                        expect(new Habit.Context().unmock).to.exist;
                    });

                    it('should take 1 argument', function () {
                        expect(new Habit.Context().unmock).to.have.length(1);
                    });

                    it('should throw an error if the object requested for unmocking does not exist', function () {
                        var shouldThrow = function () {Habit.__context.unmock('I am not real');};

                        expect(shouldThrow).to.throw(Habit.HabitError);
                    });

                    it('should call unmock on the module to be unmocked', function () {
                        supply('aMod', [], {});
                        Habit.__context.modules['aMod'].unmock = sinon.stub();
                        Habit.unmock('aMod');
                        expect(Habit.__context.modules['aMod'].unmock).to.have.been.called;
                    });
                });

                describe('unmockAll', function () {
                    it('should be defined', function () {
                        expect(Habit.__context.unmockAll).to.exist;
                    });

                    it('should unmock all currently mocked modules', function () {
                        supply('a', [], {});
                        supply('b', [], {});
                        Habit.mock('a', {});
                        Habit.mock('b', {});
                        expect(Habit.__context.modules['a'].__isMocked).to.be.true;
                        expect(Habit.__context.modules['b'].__isMocked).to.be.true;
                        Habit.__context.unmockAll();
                        expect(Habit.__context.modules['a'].__isMocked).to.be.false;
                        expect(Habit.__context.modules['b'].__isMocked).to.be.false;
                    });
                });

                describe('cloneResolveAndCall', function () {

                    var deps = ['a', 'b'], cb = sinon.stub(), overrides = {a: {}};
                    var clone = {injectOverrides: sinon.stub(), resolveAndCall: sinon.stub()};

                    it('should exist', function () {
                        expect(Habit.__context.cloneResolveAndCall).to.exist;
                    });

                    it('should call context.clone', function () {
                        Habit.__context.clone = sinon.stub().returns(clone);
                        Habit.__context.cloneResolveAndCall(deps, cb, overrides);
                        expect(Habit.__context.clone).to.have.been.called;
                    });

                    it('should call injectOverrides on the returned clone', function () {
                        Habit.__context.clone = sinon.stub().returns(clone);
                        Habit.__context.cloneResolveAndCall(deps, cb, overrides);
                        expect(clone.injectOverrides).to.have.been.called;
                    });

                    it('calls resolveAndCall on the returned clone', function () {
                        Habit.__context.clone = sinon.stub().returns(clone);
                        Habit.__context.cloneResolveAndCall(deps, cb, overrides);
                        expect(clone.resolveAndCall).to.have.been.calledWith(deps, cb);
                    });
                });

                describe('clone', function () {
                    beforeEach(function () {
                        sinon.spy(Habit, 'Context');
                    });

                    afterEach(function () {
                        Habit.Context.restore();
                    });

                    it('should exist', function () {
                        expect(Habit.__context.clone).to.exist;
                    });

                    it('should create a new context instance', function () {
                        expect(Habit.Context).not.to.have.been.called;
                        Habit.__context.clone();
                        expect(Habit.Context).to.have.been.called;
                    });

                    it('should call clone on all the existing modules', function () {
                        supply('a', [], 'wibble');
                        supply('b', [], 'wobble');

                        Habit.__context.modules['a'].clone = sinon.stub();
                        Habit.__context.modules['b'].clone = sinon.stub();

                        Habit.__context.clone();

                        expect(Habit.__context.modules['a'].clone).to.have.been.called;
                        expect(Habit.__context.modules['b'].clone).to.have.been.called;
                    });
                });

                describe('injectOverrides', function () {
                    it('should be defined', function () {
                        expect(Habit.__context.injectOverrides).to.exist;
                    });

                    it('should call mock once for each key in overrides', function () {
                        var amock = {}, bmock = {}, overrides = {'a': {}, 'b': {}},
                        context = new Habit.Context();

                        context.inject = sinon.stub();

                        context.injectOverrides(overrides);

                        expect(context.inject).to.have.been.calledWith('a', amock);
                        expect(context.inject).to.have.been.calledWith('b', bmock);
                    });
                });
            });

            describe('__context', function () {
                it('should always exist', function () {
                    expect(Habit.__context).to.exist;
                });
            });

            describe('Habit.destroy', function () {

                it('should be defined', function () {
                    expect(Habit.destroy).to.exist;
                });

                it('should destroy and recreate the context', function () {
                    var ctx = Habit.__context;
                    Habit.destroy();
                    expect(ctx).not.to.equal(Habit.__context);
                });
            });

            describe('Module', function () {
                it('should exist', function () {
                    expect(Habit.Module).to.exist;
                });

                it('should take 3 arguments', function () {
                    expect(Habit.Module).to.have.length(3);
                });

                it('should set the name to the name passed to the name property', function () {
                    expect(new Habit.Module('aName', [], {}).name).to.equal('aName');
                });

                it('should set the dependencies to the passed dependencies', function () {
                    expect(new Habit.Module('aName', ['someModule'], {}).dependencies)
                    .to.eql(['someModule']);
                });

                it('should set the originalValue to the value passed', function () {
                    var moduleValue = {};
                    expect(new Habit.Module('aName', [], moduleValue).originalValue)
                    .to.equal(moduleValue);
                });

                it('should resolve relative module names correctly', function () {
                    var dependencies = [
                        './module',
                        '../module'
                    ];

                    var module = new Habit.Module('some/fancy/thing', dependencies, {});

                    expect(module.dependencies[0]).to.equal('some/fancy/module');
                    expect(module.dependencies[1]).to.equal('some/module');
                });

                describe('mock', function () {

                    it('should be defined', function () {
                        supply('myMod', [], {});
                        expect(Habit.__context.modules['myMod'].mock).to.exist;
                    });

                    it('should cause the module to return the mock from resolve', function () {
                        var mock = {};
                        supply('myMod', [], function () {return 'wibble';});
                        Habit.__context.modules['myMod'].mock(mock);
                        expect(Habit.__context.modules['myMod'].resolve()).to.equal(mock);
                    });

                    it('should be disolving dependent modules ' +
                        'so that the mock is supplied to them when they are next resolved', function () {

                        supply('modA', [], 'wibble');
                        supply('modB', ['modA'], function (m) {return m;});
                        expect(need('modA')).to.equal('wibble');

                        Habit.mock('modA', 'boing');
                        expect(need('modA')).to.equal('boing');

                    });
                });

                describe('inject', function () {
                    it('should be defined', function () {
                        supply('mymod', [], {});
                        expect(Habit.__context.modules['mymod'].inject).to.exist;
                    });

                    it('should replace the original value', function () {
                        var original = 'original', injected = 'injected';

                        supply('mymod', [], original);

                        Habit.__context.modules['mymod'].inject(injected);

                        expect(Habit.__context.modules['mymod'].originalValue).to.equal(injected);
                    });
                });

                describe('unmock', function () {
                    it('should exist', function () {
                        supply('mymod', [], 'wibble');
                        expect(Habit.__context.modules['mymod'].unmock).to.exist;
                    });

                    it('should throw an error if the module is not mocked', function () {
                        var shouldThrow = function () {
                            supply('mymod', [], 'wibble');
                            Habit.__context.modules['mymod'].unmock();
                        };

                        expect(shouldThrow).to.throw(Habit.HabitError);
                    });

                    it('should unmock the module', function () {
                        supply('mymod', [], 'real');

                        Habit.mock('mymod', 'mocked');
                        expect(need('mymod')).to.equal('mocked');

                        Habit.unmock('mymod');
                        expect(need('mymod')).to.equal('real');
                    });

                    it('should disolve dependent modules so that' +
                    ' they now use the correct value', function () {
                        supply('moda', [], 'real');
                        supply('modb', ['moda'], function (a) {return a;});
                        expect(need('modb')).to.equal('real');

                        Habit.mock('moda', 'mocked');
                        expect(need('modb')).to.equal('mocked');

                        Habit.unmock('moda');
                        expect(need('modb')).to.equal('real');
                    });
                });

                describe('clone', function () {
                    it('should exist', function () {
                        supply('a', [], {});
                        expect(Habit.__context.modules['a'].clone).to.exist;
                    });

                    it('should create a new module with the properties of this one', function () {

                        var name = 'a', deps = ['b'], value = 'real', mockValue = 'mock';

                        sinon.spy(Habit, 'Module');

                        supply(name, deps, value);
                        Habit.mock(name, mockValue);

                        var clone = Habit.__context.modules[name].clone();

                        expect(Habit.Module).to.have.been.calledWith(name, deps, value);
                        expect(clone.__isMocked).to.be.true;
                        expect(clone.__mockedValue).to.equal('mock');
                    });
                });
            });

            describe('disolve', function () {

                beforeEach(function () {
                    supply('modb', [], {});
                    supply('moda', ['modb'], function (b) {});

                    need(['moda'], sinon.stub());
                });

                it('should exist', function () {
                    expect(new Habit.Module('aName', [], function () {}).disolve).to.exist;
                });

                it('should disolve the module', function () {

                    Habit.disolve('moda');

                    expect(Habit.__context.modules['moda'].__isResolved).to.be.false;
                    expect(Habit.__context.modules['moda'].__resolvedValue).not.to.exist;
                });

                it('should disolve all the modules which depend upon this module', function () {

                    Habit.disolve('modb');

                    expect(Habit.__context.modules['moda'].__isResolved).to.be.false;
                    expect(Habit.__context.modules['moda'].__resolvedValue).not.to.exist;
                });

                it('should remove the dependent modules from disolved modules', function () {
                    Habit.disolve('modb');

                    expect(Habit.__context.modules['modb'].dependentModules).to.have.length(0);
                });
            });

            describe('HabitError', function () {

                it('should be defined', function () {
                    expect(Habit.HabitError).to.exist;
                });

            });

            describe('disolve', function () {
                it('should be defined', function () {
                    expect(Habit.disolve).to.exist;
                });

                it('should call __context.disolve', function () {
                    Habit.__context.disolve = sinon.stub();
                    Habit.disolve('mymod');
                    expect(Habit.__context.disolve).to.have.been.called;
                });

                describe('require.undef', function () {
                    it('should exist', function () {
                        expect(require.undef).to.exist;
                    });

                    it('should be an alias of Habit.disolve', function () {
                        Habit.__context.disolve = sinon.stub();
                        require.undef('mymod');
                        expect(Habit.__context.disolve).to.have.been.called;
                    });
                });
            });

            describe('disolveAll', function () {
                it('should exist', function () {
                    expect(Habit.disolveAll).to.exist;
                });

                it('should call __context.disolveAll', function () {
                    Habit.__context.disolveAll = sinon.stub();
                    Habit.disolveAll();
                    expect(Habit.__context.disolveAll).to.have.been.called;
                });
            });

            describe('configuration objects', function () {
                describe('module', function () {
                    it('should be a special case module which is predefined for every context', function () {
                        expect(Habit.__context.modules['module']).to.exist;
                    });

                    it('should raise an exception if the user attempts to supply a module "module"', function () {
                        var shouldThrow = function () {supply('module', [], {});};
                        expect(shouldThrow).to.throw(Habit.HabitError);
                    });

                    it('should return configuration data set for a particular module', function () {
                        var myConfig = {};
                        Habit.__configData['my/module'] = myConfig;
                        supply('my/module', ['module'], function (m) {
                            expect(m).to.equal(myConfig);
                        });

                        need('my/module');
                    });

                    //This test uses config data set in the index.html file for these tests
                    it('should return configuration data set in the global namespace', function () {
                        supply('my/configured/module', ['module'], function (m) {
                            expect(m.message).to.equal('Hello from the global namespace');
                        });

                        need('my/configured/module');
                    });
                });
            });

            describe('mocking and unmocking', function () {

                var mock = {einie: 'menie'}, name = 'someMod';

                beforeEach(function () {
                    Habit.__context.mock = sinon.stub();
                    Habit.__context.unmock = sinon.stub();
                });

                describe('mock', function () {

                    it('should be defined', function () {
                        expect(Habit.mock).to.exist;
                    });

                    it('should take two arguments', function () {
                        expect(Habit.mock).to.have.length(2);
                    });

                    it('should call __context.mock', function () {
                        Habit.mock(name, mock);
                        expect(Habit.__context.mock).to.have.been.calledWith(name, mock);
                    });
                });

                describe('unmock', function () {

                    it('should be defined', function () {
                        expect(Habit.unmock).to.exist;
                    });

                    it('should have a length of 1', function () {
                        expect(Habit.unmock).to.have.length(1);
                    });

                    it('should call __context.unmock', function () {
                        Habit.unmock(name);
                        expect(Habit.__context.unmock).to.have.been.calledWith(name);
                    });

                });

                describe('unmockAll', function () {
                    it('should be defined', function () {
                        expect(Habit.unmockAll).to.exist;
                    });

                    it('should call __context.unmockAll', function (){
                        Habit.__context.unmockAll = sinon.stub();
                        Habit.unmockAll();
                        expect(Habit.__context.unmockAll).to.have.been.called;
                    });
                });
            });
        });
        describe('local', function () {

            beforeEach(function () {
                supply('a', [], 'original');
            });

            it('should exist', function () {
                var cb = function (local) {
                    expect(local).to.exist;
                };

                need(['local'], cb, {});
            });

            it('should have local.define / local.supply', function () {
                var cb = function (local) {expect(local.supply).to.exist;};
                need(['local'], cb, {});
            });

            it('should allow the definition of local modules', function () {
                var cb = function (local) {
                    local.supply('b', [], 'local-module');

                    expect(local.need('b')).to.equal('local-module');
                };

                need(['local'], cb, {});
            });

            it('should not make local modules available externally', function () {

                var cb = function (local) {local.supply('b', [], 'some-value');};
                var shouldThrow = function () {need('b');};
                expect(shouldThrow).to.throw(Habit.HabitError);

            });

            describe('local.need / local.require', function () {
                it('should exist', function () {
                    var cb = function (local) {
                        expect(local.require).to.exist;
                    };

                    need(['local'], cb, {});
                });

                it('should return the local version of a module', function () {

                    var cb = function (local) {
                        expect(local.need('a')).to.equal('injected');
                    };

                    need(['local'], cb, {'a' : 'injected'});
                });
            });

            it('should have local.mock', function () {
                var cb = function (local) {expect(local.mock).to.exist;};
                need(['local'], cb, {});
            });

            it('should mock locally', function () {
                var cb = function (local) {
                    local.mock('a', 'mocked');
                    expect(local.need('a')).to.equal('mocked');
                };

                need(['local'], cb, {});
            });

            it('should not mock externally', function () {
                var cb = function (local) {
                    local.mock('a', 'mocked');
                };

                need(['local'], cb, {});

                expect(need('a')).to.equal('original');
            });

            it('should have local.unmock', function () {
                var cb = function (local) {expect(local.unmock).to.exist;};
                need(['local'], cb, {});
            });

            it('should unmock modules locally', function () {
                var cb = function (local) {
                    local.unmock('a');
                    expect(local.need('a')).to.equal('original');
                };

                Habit.mock('a', 'mocked');

                need(['local'], cb, {});
            });

            it('should not unmock modules externally', function () {
                var cb = function (local) {local.unmock('a');};

                Habit.mock('a', 'mocked');

                need(['local'], cb, {});

                expect(need('a')).to.equal('mocked');
            });

            it('should have local.disolve', function () {
                var cb = function (local) {expect(local.disolve).to.exist;};

                need(['local'], cb, {});
            });

            it('should disolve modules locally', function () {
                var cb = function (local) {
                    local.disolve('a');
                    expect(local.__context.modules['a'].__isResolved).to.be.false;
                };

                need(['local'], cb, {});
            });

            it('should not disolve modules externally', function () {
                var cb = function (local) {local.disolve('a');};

                need('a');
                need(['local'], cb, {});

                expect(Habit.__context.modules['a'].__isResolved).to.be.true;

            });

            it('should have local.disolveAll', function () {
                var cb = function (local) {expect(local.disolveAll).to.exist;};

                need(['local'], cb, {});
            });

            it('should disolve all modules locally', function () {
                var cb = function (local, a, b) {
                    local.disolveAll();
                    expect(local.__context.modules['a'].__isResolved).to.be.false;
                    expect(local.__context.modules['b'].__isResolved).to.be.false;
                };

                supply('b', [], 'thing');

                need(['local', 'a', 'b'], cb, {});
            });

            it('should not disolve modules externally', function () {
                var cb = function (local, a, b) {local.disolveAll();};

                supply('b', ['a'], sinon.stub());

                need('b');

                need(['local', 'a'], cb, {});

                expect(Habit.__context.modules['a'].__isResolved).to.be.true;
                expect(Habit.__context.modules['b'].__isResolved).to.be.true;
            });
        });
    });
})();

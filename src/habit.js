(function () {
    'use strict';

    //Check for predifined config data
    var confObject = window.Habit || window.require || {};

    /**
    Fixjs root namespace;
    */
    var Habit = window.Habit = {};

    //set config data if any
    var configData = Habit.__configData = confObject.config || {};


    /**
    Fixjs name resolver. Given a module name resolves ./ and ../
    relative modile names
    */
    Habit.nameResolver = {
        resolveDependencyName: function (moduleName, dependencyName) {

            if (dependencyName[0] === '.') {
                return this.resolveName(
                    this.splitName(moduleName),
                    this.splitName(dependencyName));
            }

            return dependencyName;
        },

        splitName: function (name) {
            return name.split('/');
        },

        resolveName: function (moduleParts, dependencyParts) {

            moduleParts.pop();

            while(dependencyParts[0] === '..') {

                if (moduleParts.length === 0 ||
                    dependencyParts.length === 0) {

                    throw new HabitError('Illegal Relative Path');

                }

                dependencyParts.shift();
                moduleParts.pop();
            }

            if(dependencyParts[0] === '.') {
                dependencyParts.shift();
            }

            return this.completeName(moduleParts, dependencyParts);
        },

        completeName: function(moduleParts, dependencyParts) {
            dependencyParts.forEach(function(part) {
                moduleParts.push(part);
            });

            return moduleParts.join('/');
        }
    };


    /**
    Custom Error class for Fixjs
    */
    var HabitError = Habit.HabitError = function(message) {
        this.name = 'HabitError';
        this.message = message;
    };

    HabitError.prototype = new Error();
    HabitError.prototype.constructor = HabitError;


    /**
    Module. Manage state and resolution for individual modules
    */
    var Module = Habit.Module = function (name, dependencies, value) {
        this.__resolvedDependencyNames;
        this.__isResolved = false;
        this.__resolveDepth = 0;
        this.__dependentModules = [];
        this.__originalValue = value;

        Object.defineProperties(this, {
            name: {
                get: function () {return name;}
            },
            dependencies: {
                get: function () {return this.__resolvedDependencyNames.slice();}
            },
            dependentModules: {
                get: function () {return this.__dependentModules.slice();}
            },
            originalValue: {
                get: function () {return this.__originalValue;}
            }
        });

        this.__resolvedDependencyNames = this.resolveDependencyNames(dependencies);
    };

    Module.prototype.resolveDependencyNames = function (dependencies) {
        var that = this;
        return dependencies.map(function(dependency) {
            return Habit.nameResolver.resolveDependencyName(that.name, dependency);
        });
    };

    Module.prototype.resolve = function (context, dependent) {
        this.__addDependent(dependent);
        return this.__returnOrResolve(context);
    };

    Module.prototype.__returnOrResolve = function (context) {
        if(this.__isMocked) {
            return this.__mockedValue;
        }

        if(!this.__isResolved) {
            this.__resolveValue(context);
        }

        return this.__resolvedValue;
    };

    Module.prototype.__resolveValue = function (context) {
        if(typeof this.originalValue !== 'function') {
            this.__resolvedValue = this.__cloneValue(this.originalValue);
        } else {
            this.__resolvedValue = this.__resolveFactory(context);
        }
        this.__isResolved = true;
    };

    Module.prototype.__resolveFactory = function (context) {

        var that = this;

        this.__resolveDepth += 1;

        if(this.__resolveDepth > 1) {
            throw new Habit.HabitError('Curcular Dependency Detected in ' + this.name);
        }

        var resolved = this.originalValue.apply(undefined, this.__resolvedDependencyNames.map(function (dependency) {
                return context.resolveModule(dependency, that);
            }));

        this.__resolveDepth -= 1;

        return resolved;
    };

    Module.prototype.__addDependent = function (dependent) {
        if(dependent) {
            this.__dependentModules.push(dependent);
        }
    };

    Module.prototype.disolve = function () {
        this.__dependentModules.forEach(function (module) {
            module.disolve();
        });

        this.__isResolved = false;
        this.__resolvedValue = undefined;
        this.__dependentModules = [];
    };

    Module.prototype.mock = function (mock) {
        this.disolve();
        this.__isMocked = true;
        this.__mockedValue = mock;
    };

    Module.prototype.unmock = function () {
        if(!this.__isMocked) {
            throw new Habit.HabitError('"' + this.name + '" is not mocked');
        }
        this.__isMocked = false;
        this.__mockedValue = undefined;
        this.disolve();
    };

    Module.prototype.clone = function () {
        var clone = new Habit.Module(this.name, this.__resolvedDependencyNames, this.originalValue);
        clone.__isMocked = this.__isMocked;
        clone.__mockedValue = this.__mockedValue;
        return clone;
    };

    Module.prototype.inject = function (override) {
        this.disolve();
        this.__originalValue = override;
    };

    Module.prototype.__cloneValue = function (value) {

        if (Array.isArray(value)) {
            return this.__cloneArray(value);
        }

        if(typeof value === 'object') {
            return this.__cloneObject(value);
        }

        //value is primitive
        return value;
    };

    Module.prototype.__cloneObject = function (obj) {
        var clone = {};

        Object.keys(obj).forEach(function(key) {
            clone[key] = obj[key];
        });

        return clone;
    };

    Module.prototype.__cloneArray = function (arr) {
        return arr.slice();
    };




    /**
    ConfigModule
    Module to handle configuration data
    */
    var ConfigModule = Habit.ConfigModule = function () {
        Habit.Module.call(this, 'module', [], configData);
    };

    ConfigModule.prototype = new Habit.Module('module', [], configData);
    ConfigModule.prototype.constructor = ConfigModule;

    ConfigModule.prototype.resolve = function (context, dependent) {
        if(!dependent) {
            return undefined;
        }

        if(configData[dependent.name]) {
            return configData[dependent.name];
        }

        return undefined;
    };


    /**
    Context. Manage the current module graph.
    */
    var Context = Habit.Context = function () {
        this.__modules = {};

        Object.defineProperty(this, 'modules', {get: function () {return this.__modules;}});

        this.__addConfigModule();
    };

    Context.prototype.addModule = function (name, dependencies, callback) {
        if (name === 'module') {
            throw new Habit.HabitError('The module name "module" is reserved');
        }

        this.__addModule(name, dependencies, callback);
    };

    Context.prototype.__addConfigModule = function () {
        this.__modules['module'] = new Habit.ConfigModule();
    };

    Context.prototype.__addModule = function (name, dependencies, callback) {
        if(this.__modules[name]) {
            this.__modules[name].disolve();
        }
        this.__modules[name] = new Module(name, dependencies, callback);
    };

    Context.prototype.resolveModule = function (name, dependent) {
        if(!this.__modules[name]) {
            throw new Habit.HabitError('Module "' + name + '"is not defined');
        }
        return this.__modules[name].resolve(this, dependent);
    };

    Context.prototype.resolveAndCall = function(dependencies, callback) {
        var that = this;
        callback.apply(undefined, dependencies.map(function(dependency) {
            return that.resolveModule(dependency);
        }));
    };

    Context.prototype.__allModules = function () {
        var that = this;

        return Object.keys(this.__modules)
        .map(function (key) {
            return that.__modules[key];
        });
    };

    Context.prototype.disolve = function (name) {
        if(this.__modules[name]) {
            this.__modules[name].disolve();
        }
    };

    Context.prototype.disolveAll = function () {
        this.__allModules()

        .filter(function (module) {
            return module.__isResolved;
        })

        .forEach(function (module) {
            module.disolve();
        });
    };

    Context.prototype.mock = function (name, mock) {
        if(!this.__modules[name]) {
            throw new Habit.HabitError('Module "' + name + '" cannot be mocked because it does not exist');
        }
        this.__modules[name].mock(mock);
    };

    Context.prototype.inject = function (name, override) {
        if(!this.__modules[name]) {
            throw new Habit.HabitError('Module "' + name + '" cannot be injected because it does not exist');
        }
        this.__modules[name].inject(override);
    };

    Context.prototype.unmock = function (name) {
        if(!this.__modules[name]) {
            throw new Habit.HabitError('Module "' + name + '" cannot be unmocked because it does not exist');
        }
        this.__modules[name].unmock();
    };

    Context.prototype.unmockAll = function () {

        this.__allModules()

        .filter(function (module) {
            return module.__isMocked;
        })

        .forEach(function (module) {
            module.unmock();
        });
    };

    Context.prototype.cloneResolveAndCall = function (dependencies, callback, overrides) {
        var clone = this.clone();
        clone.injectOverrides(overrides);
        clone.resolveAndCall(dependencies, callback);
    };

    Context.prototype.__addLocalModule = function (clone) {
        clone.addModule('local', [], function () {
            return new Habit.Local(clone);
        });
    };

    Context.prototype.clone = function () {
        var clone = new Habit.Context();
        var that = this;

        Object.keys(this.__modules).forEach(function (key) {
            if(key !== 'module') {
                clone.__modules[key] = that.__modules[key].clone();
            }
        });

        this.__addLocalModule(clone);

        return clone;
    };

    Context.prototype.injectOverrides = function (overrides) {
        var that = this;
        Object.keys(overrides).forEach(function (key) {
            that.inject(key, overrides[key]);
        });
    };


    /**
    Local, facade for local context made available to injection closures
    **/

    var Local = Habit.Local = function (context) {
        this.__context = context;
    };

    Local.prototype.need = function (dependencies, callback, overrides) {
        return Habit.__need.bind(this.__context)(dependencies, callback, overrides);
    };

    Local.prototype.require = Local.prototype.need;

    Local.prototype.supply = function (name, dependencies, callback) {
        this.__context.addModule(name, dependencies, callback);
    };

    Local.prototype.define = Local.prototype.supply;

    Local.prototype.mock = function (name, mock) {
        this.__context.mock(name, mock);
    };

    Local.prototype.unmock = function (name) {
        this.__context.unmock(name);
    };

    Local.prototype.disolve = function (name) {
        this.__context.disolve(name);
    };

    Local.prototype.disolveAll = function () {
        this.__context.disolveAll();
    };

    Local.prototype.unmockAll = function () {
        this.__context.unmockAll();
    };


    /**
    Create the default Fixjs context object
    */
    Habit.__context = new Context();


    Habit.supply = function (name, dependencies, callback) {
        Habit.__context.addModule(name, dependencies, callback);
    };

    window.supply = Habit.supply.bind(Habit);
    window.define = Habit.supply.bind(Habit);


    /**
    concrete implementation for both need and local need.
    MUST be called bound to the correct context. i.e.
    Fixjs.__need.bind(context)(dependencies, callback, overrides);
    **/
    Habit.__need = function(dependencies, callback, overrides) { //jshint ignore: line
        if(overrides) {
            this.cloneResolveAndCall(dependencies, callback, overrides);
            return;
        }

        if(!callback) {
            return this.resolveModule(dependencies);
        }

        this.resolveAndCall(dependencies, callback);
    };


    Habit.need = function (dependencies, callback, overrides) {

        return Habit.__need.bind(Habit.__context)(dependencies, callback, overrides);
    };

    window.need = Habit.need.bind(Habit);
    window.require = Habit.need.bind(Habit);


    /**
    undefine a module and all dependent modules
    */
    Habit.disolve = function (name) {
        this.__context.disolve(name);
    };

    Habit.disolveAll = function () {
        this.__context.disolveAll();
    };

    require.undef = Habit.disolve.bind(Habit);


    Habit.destroy = function () {
        this.__context = new Habit.Context();
    };


    /**
    Mock & Unmock
    */
    Habit.mock = function (name, mock) {
        this.__context.mock(name, mock);
    };

    Habit.unmock = function (name) {
        this.__context.unmock(name);
    };

    Habit.unmockAll = function () {
        this.__context.unmockAll();
    };

})();

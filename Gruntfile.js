module.exports = function (grunt) {
    'use strict';

    require('load-grunt-tasks')(grunt);

    var sourceFiles = [
        'src/habit.js'
    ];

    grunt.initConfig({
        watch: {
            tests: {
                files: ['tests/tests/*.js'],
                tasks: ['uglify:tests', 'mocha_phantomjs:tests']
            },
            src: {
                files: ['src/*.js'],
                tasks: ['uglify:testsSrc', 'mocha_phantomjs:tests']
            }
        },
        copy: {
            fixjs: {
                expand: true,
                cwd: 'tests/js',
                src: 'habit.min.js',
                dest: 'example/js/'
            },
            blanket: {
                src: 'bower_components/blanket/dist/qunit/blanket.min.js',
                dest: 'tests/js/blanket.min.js'
            },
            blanketMocha: {
                src: 'bower_components/blanket/src/adapters/mocha-blanket.js',
                dest: 'tests/js/mocha-blanket.js'
            }
        },
        connect: {
            debugTests: {
                options: {
                    port: 9000,
                    base: ['tests'],
                    keepalive: true
                }
            },
            tests: {
                options: {
                    port: 9000,
                    base: ['tests']
                }
            },
            example: {
                options: {
                    port: 8000,
                    base: ['example'],
                    keepalive: true
                }
            }
        },
        'mocha_phantomjs': {
            tests: {
                options: {
                    urls: ['http://localhost:9000']
                }
            }
        },
        cssmin: {
            tests: {
                files: {
                    'tests/css/tests.css': ['bower_components/mocha/mocha.css']
                }
            }
        },
        uglify: {
            testsVendor: {
                files: {
                    'tests/js/vendor.min.js': [
                        'bower_components/mocha/mocha.js',
                        'bower_components/chai/chai.js',
                        'bower_components/sinon-chai/lib/sinon-chai.js',
                        'bower_components/sinonjs/sinon.js'
                    ]
                },
                options : {
                    sourceMap: true,
                    sourceMapIncludeSources: true,
                    compress: false,
                    mangle: false,
                    beautify: true
                }
            },
            tests: {
                files: {
                    'tests/js/tests.min.js': [
                        'tests/tests/habit.js'
                    ]
                },
                options : {
                    sourceMap: true,
                    sourceMapIncludeSources: true,
                    compress: false,
                    mangle: false,
                    beautify: true
                }
            },
            testsSrc: {
                files: {
                    'tests/js/habit.min.js': sourceFiles
                },
                options : {
                    sourceMap: true,
                    sourceMapIncludeSources: true,
                    compress: false,
                    mangle: false,
                    beautify: true
                }
            },
            example: {
                files: {
                    'example/js/app.min.js': [
                        'example/src/dependency.js',
                        'example/src/module.js',
                        'example/src/start.js'
                    ]
                },
                options: {
                    compress: false,
                    mangle: false,
                    beautify: true,
                    sourceMap: true,
                    sourceMapIncludeSources: true
                }
            }
        }
    });

    grunt.registerTask('default', [
        'uglify:testsVendor',
        'copy:blanket',
        'copy:blanketMocha',
        'cssmin:tests',
        'uglify:tests',
        'uglify:testsSrc',
        'connect:tests',
        'mocha_phantomjs:tests',
        'watch'
    ]);

    grunt.registerTask('build:example', ['copy:fixjs', 'uglify:example']);
};
